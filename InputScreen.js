import React from "react";
import {View, Text, StyleSheet, TouchableOpacity, Vibration} from "react-native";
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import DoubleClick from "react-native-double-tap";
import {activateKeepAwake} from "expo-keep-awake";


/*Constants for URLS */
//const MAINURL = "http://MobilePrediction/api/"

//for mobile teting
//const MAINURL = "http://192.168.0.100/api/"


//for testing on heroku
const MAINURL = "https://peopleprediction.herokuapp.com/api/"


export default class InputScreen extends React.Component {
    state = {
     myText: 'I\'m ready to get swiped!',
     gestureName: 'none',
     backgroundColor: '#fff'
   };

   componentDidMount() {
     activateKeepAwake()
   }


 async onSwipeRight(gestureState) {
  //  let test = await this.turnOffApp()
  //  if(test)
  //  {
  //    this.props.screenProps.settings.app_status = false
  //    this.props.navigation.navigate("homepage")
   //
  //  }

  await this.turnOffApp()
  //console.log("swiped right");

 }


 //Turns off the app
 turnOffApp = async() => {
   //remove any errors if there is any
   this.setState({
     errorFlag : false,
   })
   try
   {
     let response = await fetch(`${MAINURL}deactivate`, {
       method : "POST",
       header : {
         "Content-Type" : "application/json",
         "Accept" : "application/json"
       },
       body : JSON.stringify({
         "api_token" : this.props.screenProps.api_token,
       }),
     })

     //console.log("status:", response.status)
     let result = await response.json()
     //console.log("Turned Off")
     //console.log(result)
     if(result.activated === false)
     {
       this.props.navigation.navigate("homepage")
       //return true
     }

     /*bug fix, have to change status in screenProps too otherwise when
     switching from settings to this page, this page rerenders with previous
     screenprops */
     this.props.screenProps.settings.app_status = false



   }catch (e)
   {
     //display error on screen
     //console.log("something went wrong in turning off app from input screen")
   }
 }


 doubleTapped = async(cardlocation) => {
   //console.log(cardlocation, "clicked", "value", this.props.screenProps.settings[cardlocation])
   //sending the value attached to that card in db
   let response = await fetch(`${MAINURL}sendInput`, {
     method : "POST",
     header : {
       "Content-Type" : "application/json",
       "Accept" : "application/json",
     },
     body : JSON.stringify({
       "api_token" : this.props.screenProps.api_token,
       "name" : this.props.screenProps.settings[cardlocation],
     }),
   })

   let result = await response.json()
   if(result.inputSaved)
   {
     //VIBRATE THE PHONE TO LET USER KNOW INPUT RECIEVED BY SPECTATORS PHONE
     Vibration.vibrate(1000)
   }
 }


  render() {

    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };

    return(
      <View style = {styles.container}>
        <View style = {styles.row}>
          <View>
            <GestureRecognizer
                onSwipeRight={(state) => this.onSwipeRight(state)}
                config={config}
                style={{
                  flex: 1,
                  justifyContent : "center",
                  alignItems : "center",
                  backgroundColor : "black"
                }}
                >
                <DoubleClick
                  doubleTap = {()=>{this.doubleTapped("card_1")}}
                  delay = {400}>
                    <View style = {styles.card1}></View>
                </DoubleClick>
            </GestureRecognizer>
          </View>
          <View >
            <DoubleClick
              doubleTap = {()=>{this.doubleTapped("card_2")}}
              delay = {400}>
                <View style = {styles.card2}></View>
            </DoubleClick>
          </View>
        </View>
        <View style = {styles.row}>
          <View>
            <DoubleClick
              doubleTap = {()=>{this.doubleTapped("card_3")}}
              delay = {400}>
                <View style = {styles.card3}></View>
            </DoubleClick>
          </View>
          <View>
            <DoubleClick
              doubleTap = {()=>{this.doubleTapped("card_4")}}
              delay = {400}

              >
                <View style = {styles.card4}>

                </View>


            </DoubleClick>
          </View>


        </View>

      </View>
    )
  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center",
  },

  row : {
    flexDirection : "row",
    //height : 300,

  },

  card1 : {
    backgroundColor : "#000",
    width : 400,
    borderColor : "#fff",
    borderWidth : 5,
    height : 600,

  },

  card2 : {
    width : 400,
    backgroundColor : "#000",
    borderColor : "#fff",
    borderWidth : 5,
    height : 600,
  },

  card3 : {
    width : 400,
    backgroundColor : "#000",
    borderColor : "#fff",
    borderWidth : 5,
    height : 600,
  },

  card4 : {
    width : 400,
    //alignItems : "stretch",
    backgroundColor : "black",
    borderColor : "white",
    borderWidth : 5,
    height: 600,


  },
  test : {
    flex : 1,
    backgroundColor : "pink",
    //alignSelf : "stretch",
    height : 250,
    // width : "20%",
  }
})
