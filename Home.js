import React from "react";
import {Text, View, StyleSheet, Button, AsyncStorage, TouchableOpacity} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

/*Constants for URLS */
//const MAINURL = "http://MobilePrediction/api/"

//for mobile teting
//const MAINURL = "http://192.168.0.100/api/"


//for testing on heroku
const MAINURL = "https://peopleprediction.herokuapp.com/api/"



export default class Home extends React.Component {

  state = {
    "api_token" : this.props.screenProps.api_token,
    "settings" : this.props.screenProps.settings,
    "errorFlag" : false,
    "url" : this.props.screenProps.url,
    "app_status" : this.props.screenProps.settings ? this.props.screenProps.settings.app_status : 0

  }

  static navigationOptions = {
    tabBarIcon : ({tintColor, focused}) => (
      <Ionicons name = "ios-home"  size = {30} color = {tintColor}/>
    ),


  }

/*BUg FIX.. In tab navigators the screen only renders once with the screenProps
,then if screen props changes screen was not re rendering.This gave a problem in
homepage to do the logic for displaying Launch button and weblink in render if
user had no settings saved. Found this solution at
https://github.com/react-navigation/react-navigation/issues/1171 by sherifelkelesh
this what i understood is changing the sate everytime that screen is loaded
hencs forcing a render */
componentDidMount() {

this._navListener = this.props.navigation.addListener('didFocus', () => {
// get your new data here and then set state it will rerender

  this.setState({
    settings : this.props.screenProps.settings,
    url : this.props.screenProps.url,
    "app_status" : this.props.screenProps.settings ? this.props.screenProps.settings.app_status : 0
  })
  //console.log("Settings in home:", this.state.settings)
});
}

/*Bug FIX the above componentDidMount function with listener was causing a warning
of "Can't perform a React state update on an unmounted component."
this fixes it solution found reading the docs at
https://reactnavigation.org/docs/3.x/function-after-focusing-screen/#triggering-an-action-with-a-didfocus-event-listener*/
componentWillUnmount() {
    // Remove the event listener added in componentDidMount
    this._navListener.remove();
  }



//For DEbugging
  clearStorage = async() => {
    AsyncStorage.clear()
  }

//for debugging
  displayApi = async () => {
    try {
      const api = await AsyncStorage.getItem('api_token')
      if(api !== null)
      {
          //console.log("Api token in homepage is:", api)
      }
      else
      {
        //console.log("Api token is homepage is:", api)
      }

    }catch(e)
    {
      //console.log("could not get api token", this.state.api_token)
    }
  }

  //for debugging
  displayScreenProps = () => {
    //console.log("setting in screenProps are:", this.props.screenProps.settings)

  }

  //for debuggig
  displaysettings = () => {
    //console.log("this.state.setting:", this.state.settings)
  }





  launchInputScreen = async () => {
    //console.log("input screen to be launched")

    //remove the error if there is any
    this.setState({
      errorFlag : false
    })

    //activate app by changing app_status to true in DB\
    let activated = await this.activateApp()
    if(activated)
    {
      //console.log("activated", activated);
      //change launch button to OFF button
      this.setState({
        app_status : true
      })

      //change in screenProps too
      //this.props.screenProps.settings.app_status = false

      //load Input Screen
      this.props.navigation.navigate("input")
    }


  }

  activateApp = async () => {
    try {
      let response = await fetch(`${MAINURL}activate`, {
        method : "POST",
        header : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          "api_token" : this.state.api_token,
        }),
      })

      //console.log("status:", response.status)
      let result = await response.json()
      //console.log(result.activated)
      return result.activated



    } catch (err) {
      //display error on screen
      this.setState({
        "errorFlag" : true,
      })
    }




  }


  //Turns off the app
  turnOffApp = async() => {
    //remove any errors if there is any
    this.setState({
      errorFlag : false,
    })
    try
    {
      let response = await fetch(`${MAINURL}deactivate`, {
        method : "POST",
        header : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          "api_token" : this.state.api_token,
        }),
      })

      //console.log("status:", response.status)
      let result = await response.json()
      //console.log("Turned Off")
      this.setState({
        "app_status" : result.activated
      })

      /*bug fix, have to change status in screenProps too otherwise when
      switching from settings to this page, this page rerenders with previous
      screenprops */
      this.props.screenProps.settings.app_status = false

    }catch (e)
    {
      //display error on screen
      this.setState({
        "errorFlag" : true,
      })
    }
  }


  render(){
    /*Bug fix to logic of displaying launch button, if settings
    object has length > 0 then display it other wise not*/
    let flag
    if(this.props.screenProps.settings)
    {
      flag = Object.keys(this.props.screenProps.settings).length
      //console.log(flag)
    }
    else
    {
      flag = 0
    }

    return (
      <View style = {styles.container}>
        <Text style = {{fontSize : 18, fontWeight: "bold"}}>Homepage</Text>
        <Text style = {{fontSize : 18}}>Welcome {this.props.screenProps.name}</Text>

        {
          (flag > 0) ?

            (
              <View>
              <View style={styles.url}>
                <Text>Your website is at </Text>
                <Text style = {styles.link}>{this.props.screenProps.url}</Text>

              </View>
              <View style ={styles.button}>
                <Button
                  title = {this.state.app_status ? "Turn OFF" : "Launch"}
                  color = {this.state.app_status ? "grey" : "green"}
                  onPress = {this.state.app_status ? this.turnOffApp : this.launchInputScreen}/>
              </View>
              </View>

            )

          :

          (
            <View style = {styles.messgToSaveSettings}>
              <View style ={{flexDirection : "row"}}>
                <Text>First please visit the info </Text>
                <Ionicons name = "ios-information-circle" size = {20} color = "grey"/>
                <Text> page and learn how the app works.</Text>
              </View>
              <View style ={{flexDirection : "row"}}>
                <Text>Then save some names in settings </Text>
                <Ionicons name = "md-settings" size = {20} color = "grey"/>
                <Text> page and come back to launch the app.</Text>
              </View>
            </View>

          )
      }


      {
        this.state.errorFlag ? (
          <View style = {styles.error}>
            <Text style = {{color : "red"}}>Something went wrong please try later</Text>
          </View>
        )
        : null
      }


      </View>

    )
  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center",
    backgroundColor : "#fff"
  },

  url : {
    //fontSize : 20,
    flexDirection : "row",
    padding : 10
  },

  link : {
    color : "blue",
    textDecorationLine : "underline"
  },

  button : {
    padding : 15,
    alignItems : "center"
  },

  messgToSaveSettings : {
    padding : 10,

  },

  error : {
    padding : 8,

  },


})
