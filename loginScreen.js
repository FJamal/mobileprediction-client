import React from "react";
import {View, TextInput, Button, StyleSheet, Text, TouchableOpacity, AsyncStorage, Alert} from "react-native";
import {Linking} from "expo";


/*Constants for URLS */
// const MAINURL = "http://MobilePrediction/api/"
// const PASSRESET = "http://MobilePrediction/password/reset"

//for mobile teting
//const MAINURL = "http://192.168.0.100/api/"
//const PASSRESET = "http://192.168.0.100/password/reset"


//for testing on heroku
const MAINURL = "https://peopleprediction.herokuapp.com/api/"
const PASSRESET = "http://peopleprediction.herokuapp.com/password/reset"



export default class Login extends React.Component {

  state = {
    email : "",
    password : "",
    serverError : false,
    serverErrorMessage : "",
  }





  loadRegister = () => {
    this.props.navigation.navigate("register")
  }


  handleSubmit = async() => {
    const email  = this.state.email
    const pass = this.state.password


    //const proxyurl = "https://cors-anywhere.herokuapp.com/";
    /*TEST FOR GET REQUEST */
    // let response = await fetch(`http://MobilePrediction/user?email=${email}&pass=${pass}`, {
    //   headers: {
    //      "Content-Type" : "application/json",
    //      "Accept" : "application/json"
    //    },
    //
    // })

    /*TEST FOR POST REQUEST */
    let response = await fetch(`${MAINURL}login`, {
      method: "POST",
      header : {
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      },
      body : JSON.stringify({
        "email" : email,
        "password" : pass
      }),
    })

    let result = await response.json()
    //console.log(result)

    if(result.serverError)
    {
      this.setState({
        serverErrorMessage : result.serverErrorMessage,
        serverError : result.serverError,
      })
    }
    else if(!result.serverError)
    {
      //LOGS IN THE USER AND REMEMBER API TOKEN IN MOBILE MEMORY

      await this.storeData(result.api_token)

      /*Bug fix: when signing in for the first time with an already
      created acount, the homepage was not showing the api_token*/
      this.props.screenProps.api_token = result.api_token
      this.props.screenProps.name = result.name
      this.props.screenProps.url = result.url

      //TEST FOR HOMEPAGE FOR LAUNCH BUTTON LOGIC
      this.props.screenProps.settings = result.settings

      this.props.navigation.navigate("homepage")
    }

  }



  //stores the api_token in memory when user logs in for the first time
  storeData = async (api_token) => {
      try
      {
        await AsyncStorage.setItem('api_token', api_token)
      } catch(e)
      {
        //console.log("Something happened while storing in memory")
      }
  }



  /*Loads a webpage to reset password, if forgotten*/
  loadPassReset = () => {
    Linking.openURL(PASSRESET)
  }

  /*loads an alert button*/
  loadAlert = () =>{
    Alert.alert(
      "Password Resets Unavailable",
      "Since this is a prototype, this feature is not available on this app",
      [
        {
          text : "Ok",
          // style : "ok",
          // onPress : () => console.log("pressed")
        }
      ],
      {cancelable : false}
    );
  }

  emailChange = (value) => {
    this.setState({
      email : value
    })
  }

  passChange = (value) => {
    this.setState({
      password : value
    })
  }

  render(){
    return (
      <View style = {styles.container}>
        <View>
          <Text style = {{fontSize : 20, fontWeight : "bold"}}>Log In</Text>
          <TextInput
            style = {styles.input}
            placeholder = "Enter Email"
            onChangeText = {this.emailChange}
            keyboardType = "email-address"/>

          <TextInput
            style = {styles.input}
            placeholder = "Enter Password"
            onChangeText = {this.passChange}
            secureTextEntry = {true}/>

            <TouchableOpacity style = {styles.loginRegister} onPress = {this.loadRegister}>
              <Text style ={styles.register}>Register</Text>
            </TouchableOpacity>

            {this.state.error ? (<Text style = {styles.error}>Invalid email or password</Text>) : null}
            {this.state.serverError ? (<Text style = {styles.error}>{this.state.serverErrorMessage}</Text>) : null}

            <View style = {styles.loginButtonRow}>
              <Button title = "Log In" onPress = {this.handleSubmit}/>
              <TouchableOpacity
                style ={{marginLeft : 10}}
                onPress = {this.loadAlert}>
                <Text style = {{color : "blue"}}>Forgotten password?</Text>
              </TouchableOpacity>
            </View>



        </View>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  input : {
    padding : 10,
    borderBottomWidth:  2,
    borderColor : "grey",
    marginTop : 15,
  },

  loginRegister : {
    paddingBottom: 10,
    paddingTop : 10,
    alignSelf : "flex-end"
  },

  error :{
    color: "red",
    padding: 4,

  },

  loginButtonRow : {
    flexDirection : "row",
    alignItems : "center",
    justifyContent : "center",
  },

  register : {
    color : "blue",
    fontWeight: "bold",
    fontSize : 16
  },
})
