import React from "react";
import {View, Button, Text, StyleSheet, AsyncStorage} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";


export default class Logout extends React.Component {
  static navigationOptions = {
    tabBarIcon : ({tintColor, focused}) => (
      <Ionicons name = "ios-exit"  size = {30} color = {tintColor}/>
    )
  }

  logout = () => {
    //remove api_token
    AsyncStorage.clear()

    /*Bug fix: Since the whole app is loaded with a api token
    the above AsyncStorage.clear() code only removes the api token
    if the app is rerendered so if you logout and then register with
     a new account, the homepage of that account was getting the previous api_token
     and loading settings of that api-token so at logout need to change
     api_token as well*/
    this.props.screenProps.api_token = ""
    this.props.screenProps.settings = {}

    //go to login page
    this.props.navigation.navigate("login")
  }


  cancel = () => {
    this.props.navigation.navigate("home")
  }


  render(){
    return(
      <View style = {styles.container}>
        <View style = {styles.logoutBanner}>
          <Text style ={{fontSize : 18}}>Are You sure You want to logout?</Text>
          <View style = {{flexDirection : "row", justifyContent : "space-evenly"}}>
            <Button
              title = "Yes"
              color = "blue"
              onPress = {this.logout}/>

            <Button
              title = "No"
              color = "grey"
              onPress = {this.cancel}/>
          </View>
        </View>
      </View>

    )
  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    justifyContent : "center",
    alignItems : "center",
    backgroundColor : "#B0C4DE"
  },
  logoutBanner : {
    backgroundColor : "white",
    height : "50%",
    width : "50%",
    justifyContent : "space-around",
    //alignItems : "center"
  }
})
