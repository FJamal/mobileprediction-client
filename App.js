import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import Loginscreen from "./loginScreen";
import Registerscreen from "./registerscreen";
import {createSwitchNavigator, createBottomTabNavigator, createAppContainer} from "react-navigation";
import Home from "./Home.js";
import Settings from "./Settings.js";
import Info from "./Info.js";
import InputScreen from "./InputScreen.js";
import Logout from "./logout.js";

/*Constants for URLS */
//const MAINURL = "http://MobilePrediction/api/"

//for mobile teting
//const MAINURL = "http://192.168.0.100/api/"


//for testing on heroku
const MAINURL = "http://peopleprediction.herokuapp.com/api/"
const PASSRESET = "http://peopleprediction.herokuapp.com/password/reset"



//main page for the app
const MainTabs = createBottomTabNavigator({
  "home" : Home,
  "settings" : Settings,
  "info" : Info,
  "logout" : Logout
},
  {
    tabBarOptions : {
      showLabel : false
    }
  })


//login register page with login as initial page
const AppNavigator = createSwitchNavigator({
  "login" : Loginscreen,
  "register" : Registerscreen,
  "homepage" : MainTabs,
  "input" : InputScreen,
},
  {
    initialRouteName : "login"
  })

//TEST
//login register page with homepage as initial page to indicate user logged in
const AppNavigator2 = createSwitchNavigator({
  "login" : Loginscreen,
  "register" : Registerscreen,
  "homepage" : MainTabs,
  "input" : InputScreen,
},
  {
    initialRouteName : "homepage"
  })


const AppContainerLogin = createAppContainer(AppNavigator);
const AppContainerLogged = createAppContainer(AppNavigator2);



export default class App extends React.Component {

  state = {
    "api_token" : "",
    "name" : "",
    "settings" :null,
    "url" : "",
    "flag" : false,
    "serverError" : false,
  }

  storeData = async (dataToStore) => {
    try{
    await AsyncStorage.setItem('api_token', dataToStore);
    } catch (error) {
      //console.log("Could not store data");
    }
  }

  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('api_token');
      if (value !== null) {
        this.setState({
          "api_token" : value
        })
      }
      else
      {
        this.setState({
          "api_token" : ""
        })
      }

      //console.log("value of api token in App.js returned from async is", value)

    } catch (error) {
      //console.log("could not get data")
    }
  };


  async componentDidMount() {
    //AsyncStorage.clear()
    await this.retrieveData()

    //console.log("component did mount working")
    //console.log("token in state", this.state.api_token)
    /*if api_token is there that means user has already logged in once
    from this device so no need to log in again*/
    if(this.state.api_token)
    {

      /*tried changing route by this.props.navigation.navigate but
      could nt hence the if else solution in render()*/

      //instead get the users settings from here this if
      try{
        let response = await fetch(`${MAINURL}getdata`, {
          method : "POST",
          header : {
            "Content-Type" : "application/json",
            "Accept" : "application/json"
          },
          body : JSON.stringify({
            "api_token" : this.state.api_token
          }),
        })

        let result = await response.json()
        //console.log("Settings gotten from DB: " , result)

        if(result.setting)
        {
          //if gotten a response with the settings of user
          this.setState({
            name: result.userName,
            settings : result.setting,
            url : result.url,
            flag : true
          })
        }
        else
        {

          this.setState({
            name: result.userName,
            settings : [],
            flag : true,
            url : result.url

          })
        }

      } catch(e)
      {
        //could not connect to the server
        this.setState({
          serverError : true,
        })
      }




    }

  }




  render(){
    if(this.state.api_token)
    {
      if(this.state.flag)
      {
        return (
          <AppContainerLogged
              screenProps = {{
                "api_token" : this.state.api_token,
                "name": this.state.name,
                "settings" : this.state.settings,
                "url" : this.state.url,
              }}/>
          )
      }
      else
      {
        return (<View style = {styles.container}>
                  <Text>loading</Text>
                  {this.state.serverError ? (<Text style = {{backgroundColor : "black", color : "white"}}>no connection or could not retrieve data.Please try again later</Text>) : null}
                </View>)
      }



    }
    else
    {
      return (
        <AppContainerLogin
          screenProps = {{
            "api_token" : this.state.api_token,


          }}/>
      );
    }

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
