import React from "react";
import {View, TextInput, Button, StyleSheet, Text, TouchableOpacity, AsyncStorage} from "react-native";

/*Constants for URLS */
//const MAINURL = "http://MobilePrediction/api/"

//for mobile teting
//const MAINURL = "http://192.168.0.100/api/"


//for testing on heroku
const MAINURL = "https://peopleprediction.herokuapp.com/api/"


export default class Register extends React.Component {

  state = {
    flagForLogIn : true,
    name: "",
    email : "",
    password : "",
    confirmation : "",
    stopsubmission: true,
    emailError : false,
    nameError : false,
    passwordError : false,
    confirmationError : false,
    emailErrorMessage : "",
    confirmationErrorMessage : "",
    passwordErrorMessage : "",
    serverError : "",
    appPassword : "",
  }


  /*used to store api token in memory*/
  storeData = async (dataToStore) => {
    try{
    await AsyncStorage.setItem('api_token', dataToStore);
    } catch (error) {
      //console.log("Could not store data");
    }
  }




  handleRegister = () => {
    this.setState(previousstate => ({
      flagForLogIn : !previousstate.flagForLogIn
    }))
  }


  nameChange = (text) => {
    this.setState({
      name: text
    })
  }

  /*Check if this field is not empty*/
  validateName = () => {
    if(this.state.name.length === 0)
    {
      this.setState({
        nameError : true,
        nameErrorMessage : "This should be empty",
        stopsubmission : true,
      })
    }
    else
    {
      this.setState({
        nameError : false,
        nameErrorMessage : "",
        stopsubmission : false,
      })
    }
  }


  emailChange = (value) => {
    this.setState({
      email : value
    })
  }


  /*Validate the email id by checking presence of @ sign*/
  validateEmail = () => {
    if(this.state.email.search("@") === -1)
    {
      this.setState({
        emailError: true,
        emailErrorMessage : "Please enter a valid email",
        stopsubmission : true
      })
    }
    else
    {
      this.setState({
        emailError: false,
        emailErrorMessage : "",
        stopsubmission : false
      })
    }
  }


  passwordChange = (value) => {
    this.setState({
      password : value
    })
  }

  /*Check password for atleast 6 chars long*/
  validatePassword = () => {
    if(this.state.password.length < 6)
    {
      this.setState({
        passwordError : true,
        passwordErrorMessage : "Password must be atleast 6 characters long",
        stopsubmission: true,
      })
    }
    // else if(this.state.password !== this.state.confirmation)
    // {
    //   this.setState({
    //     stopsubmission : true,
    //     confirmationError : true,
    //     confirmationErrorMessage : "Passwords do not match"
    //   })
    // }
    else
    {
      this.setState({
        passwordError : false,
        passwordErrorMessage : "",
        stopsubmission: false,
      })
    }
  }


  confirmationChange = (value) => {
    this.setState({
      confirmation : value,
    })
  }


  appPassChange = (value) => {
    this.setState({
      appPassword : value,
    })
  }


  /*Checks if password matches*/
  validateConfirmationPass = () => {
    if(this.state.password === this.state.confirmation && this.state.confirmation.length !== 0)
    {
      this.setState({
        confirmationError : false,
        stopsubmission: false,
        confirmationErrorMessage : ""
      })
    }
    else
    {
      this.setState({
        confirmationError : true,
        stopsubmission: true,
        confirmationErrorMessage : "Passwords do not match"
      })
    }
  }

  loadLogin = () => {
    this.props.navigation.navigate("login")
  }




  /*Submit the registration*/
  handleSubmit = async() => {
    let response = await fetch(`${MAINURL}register` , {
      method : "POST",
      header : {
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      },
      body : JSON.stringify({
        "name" : this.state.name,
        "email" : this.state.email,
        "password" : this.state.password,
        "password_confirmation" : this.state.confirmation,
        "appPassword" : this.state.appPassword,
      })
    })

    let result = await response.json()
    //console.log(result)
    //checking if server sends an error after validation defined in ApiController
    if(result.serverError)
    {
      this.setState({
        serverError : result.serverErrorMessage,
      })
    }
    else if(!result.serverError)
    {
      //STORE THE API TOKKEN IN MOBILE MEMORY & AND LOG INTO THE APP
      this.storeData(result.api_token)
      this.props.screenProps.name = result.name
      this.props.screenProps.api_token = result.api_token
      this.props.screenProps.url = result.url

      this.props.navigation.navigate("homepage")
    }

  }

  render(){
    return (
      <View style = {styles.container}>
        <View>
          <Text style = {{fontSize : 20, fontWeight : "bold"}}>Register</Text>
          <TextInput
            style = {styles.input}
            placeholder = "Enter Full Name"
            onChangeText = {this.nameChange}
            onBlur = {this.validateName}/>

          {this.state.nameError ? (<Text style = {styles.error}>{this.state.nameErrorMessage}</Text>) : null}

          <TextInput
            style = {styles.input}
            placeholder = "Enter Email"
            onChangeText = {this.emailChange}
            onBlur = {this.validateEmail}
            keyboardType = "email-address"/>

          {this.state.emailError ? (<Text style = {styles.error}>{this.state.emailErrorMessage}</Text>) : null}

          <TextInput
            style = {styles.input}
            placeholder = "Enter Password"
            onChangeText = {this.passwordChange}
            secureTextEntry = {true}
            onBlur = {this.validatePassword}/>

          {this.state.passwordError ? (<Text style = {styles.error}>{this.state.passwordErrorMessage}</Text>) : null }


          <TextInput
            style = {styles.input}
            placeholder = "Confirm Password"
            onChangeText = {this.confirmationChange}
            secureTextEntry = {true}
            onBlur  = {this.validateConfirmationPass}/>


          <TextInput
            style = {styles.input}
            placeholder = "App Password"
            onChangeText = {this.appPassChange}
            />

          {this.state.confirmationError ? (<Text style = {styles.error}>{this.state.confirmationErrorMessage}</Text>) : null}
          {this.state.serverError ? (<Text style = {styles.error}>{this.state.serverError}</Text>) : null}

            <TouchableOpacity style = {styles.loginRegister} onPress = {this.loadLogin}>
              <Text style ={{color : "blue"}}>Log In</Text>
            </TouchableOpacity>
            <Button title = "Register" onPress = {this.handleSubmit} disabled = {this.state.stopsubmission}/>
        </View>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  input : {
    padding : 10,
    borderBottomWidth:  2,
    borderColor : "grey",
    marginTop : 15,
  },

  loginRegister : {
    paddingBottom: 10,
    paddingTop : 10,
    alignSelf : "flex-end"
  },

  error :{
    color: "red",
    padding: 4,

  }
})
