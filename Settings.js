import React from "react";
import {Text, View, StyleSheet, TextInput, Button} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";


/*Constants for URLS */
//const MAINURL = "http://MobilePrediction/api/"

//for mobile teting
//const MAINURL = "http://192.168.0.100/api/"


//for testing on heroku
const MAINURL = "https://peopleprediction.herokuapp.com/api/"


export default class Settings extends React.Component {

  state = {
    card1 : "",
    card2 : "",
    card3 : "",
    card4 : "",
    serverError : false,
    serverErrorMessage : "",
    url : "",
  }


  static navigationOptions = {
    tabBarIcon : ({focused, tintColor}) => (
    <Ionicons name = "md-settings" size = {30} color = {tintColor} />
  )

  }

  //to get the setting of the user if he had already saved one
  async componentDidMount () {
    let response  = await fetch(`${MAINURL}getdata`, {
      method : "POST",
      header : {
        "Content-Type" : "application/json",
        "Accept" : "application/json"
      },
      body : JSON.stringify({
        "api_token" : this.props.screenProps.api_token
      }),
    })

    let result = await response.json()
    //console.log("Settings in settings page:" , result)

    if(result.setting)
    {
      this.setState({
        card1 : result.setting.card_1,
        card2 : result.setting.card_2,
        card3 : result.setting.card_3,
        card4 : result.setting.card_4,

      })
    }

    this.setState({
      url : result.url
    })
  }


  /*Handle all the cards input value using another param passed from TextInput as a name*/
  // handleAllCardsInput = (value, textInputName) => {
  //
  //   //remove the error message as soon as user starts typing also and update card value
  //   this.setState({
  //     textInputName : value,
  //     serverError : false
  //   })
  //   console.log("TextInput name:" + textInputName + " Value passed:" + value)
  // }


  /*Handle all the cards input value using another param passed from TextInput as a name*/
  handleAllCardsInput = (inputname) => {

    //solution found at https://stackoverflow.com/questions/46217760/how-to-multiple-textinput-handle-with-single-handler
    return (value) => {
      this.setState({
        [inputname] : value
      })
    }
  }


  saveSettings = async() => {
    //console.log(this.state)
    try {
      let response = await fetch(`${MAINURL}savesettings` , {
        method : "POST",
        header : {
          "Content-Type" : "application/json",
          "Accept" : "application/json"
        },
        body : JSON.stringify({
          "card_1" : this.state.card1,
          "card_2" : this.state.card2,
          "card_3" : this.state.card3,
          "card_4" : this.state.card4,
          "api_token" : this.props.screenProps.api_token,
        }),
      })

      let result = await response.json()
      //console.log(result)
      if(result.serverError)
      {
        this.setState({
          serverError : true,
          serverErrorMessage : result.serverErrorMessage
        })
      }
      else
      {
        this.setState({
          serverError : false,
          serverErrorMessage : "",
        })


        /*BUGS FIX*/
        /*before navigating to the homepage update the screenProps cuz they
        dont were not updating. They only are updated when the whole is mounted in App.js*/
        this.props.screenProps.settings = {
            "card_1" : this.state.card1,
            "card_2" : this.state.card2,
            "card_3" : this.state.card3,
            "card_4" : this.state.card4,
            "app_status" : false,
          }

        /*If user had no settings saved then from App.js settings would be
        send as null to homepage and homepage would have that on the first render
        same thing with the screenprops.url,
        and since navigating from the settings page to homepage that is caused
        by saving a setting doesnot cause homepage to rerender, screenprops.url
        would still be empty and url of app at homepage would not be showed
        hence the below fix */
        this.props.screenProps.url = this.state.url

        /*END OF BUG FIX*/


        // after saving the settings Navigate to homepage
        this.props.navigation.navigate("home")
      }

    }catch (e) {
      //if not connected to server or something
      //console.log("Something went wrong Please try again")
      this.setState({
        serverError : true,
        serverErrorMessage : "Something went wrong Please try again"
      })
    }


  }




  render(){
    return (
      <View style = {styles.container}>
        <Text style = {{fontWeight: "bold" , paddingBottom : 5,}}>Settings Page</Text>
        <View style = {styles.inputSection}>
          <View style = {styles.inputLabelLine}>
            <Text style = {{padding : 5}}>Card 1</Text>
            <View style = {{marginLeft : 10}}>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconHighlighted}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
            </View>
          </View>
          <View>
            <TextInput
              style = {styles.input}
              onChangeText = {this.handleAllCardsInput("card1")}
              value = {this.state.card1}/>
          </View>


        </View>



        <View style = {styles.inputSection}>
          <View style = {styles.inputLabelLine}>
            <Text style = {{padding : 5}}>Card 2</Text>
            <View style = {{marginLeft : 10}}>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconHighlighted}></View>
              </View>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
            </View>
          </View>

          <TextInput
            style = {styles.input}
            onChangeText = {this.handleAllCardsInput("card2")}
            value = {this.state.card2}/>
        </View>



        <View style = {styles.inputSection}>
          <View style = {styles.inputLabelLine}>
            <Text style = {{padding : 5}}>Card 3</Text>
            <View style = {{marginLeft : 10}}>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconHighlighted}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
            </View>
          </View>
          <TextInput
            style = {styles.input}
            onChangeText = {this.handleAllCardsInput("card3")}
            value = {this.state.card3}/>
        </View>



        <View style = {styles.inputSection}>
          <View style = {styles.inputLabelLine}>
            <Text style = {{padding : 5}}>Card 4</Text>
            <View style = {{marginLeft : 10}}>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconHighlighted}></View>
              </View>
            </View>
          </View>
          <TextInput
            style = {styles.input}
            onChangeText = {this.handleAllCardsInput("card4")}
            value = {this.state.card4}/>
        </View>

        {this.state.serverError ? (<Text style ={styles.error}>{this.state.serverErrorMessage}</Text>) : null}
        <View style = {styles.button}>
          <Button title = "Save" onPress = {this.saveSettings} />
        </View>


      </View>
    )
  }
}


const styles = StyleSheet.create({
  container : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center",
    backgroundColor : "#fff"
  },

  input : {
    padding : 10,
    borderBottomWidth:  2,
    borderColor : "grey",
    marginTop : 15,

  },

  button : {
    padding : 7,
  },

  error :{
    color: "red",
    padding: 4,

  },

  inputIconRow : {
    flexDirection : "row"
  },

  inputIconSection : {
    width : 13,
    height : 13,
    backgroundColor : "black",
    borderWidth : 1,
    borderColor : "white",
  },

  inputIconHighlighted : {
    width : 13,
    height : 13,
    backgroundColor : "blue",
    borderWidth : 1,
    borderColor : "white",
  },

  inputLabelLine : {
    flexDirection : "row",

  },

  inputSection : {
    marginTop : 5,
    width: "50%"
  }

})
