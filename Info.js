import React from "react";
import {Text, View, StyleSheet, ScrollView, Image, TouchableOpacity} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import {Linking} from "expo";
import Constants from "expo-constants";

export default class Info extends React.Component {

  static navigationOptions = {
    tabBarIcon : ({focused, tintColor}) => (
      <Ionicons name = "ios-information-circle" size = {30} color = {tintColor}/>
    )
  }

  crimpVideo = () => {
    //Linking.openURL("https://www.youtube.com/watch?v=N3xXhDBf-M0");
  }
  render(){
    return (

      <ScrollView contentContainerStyle = {styles.container}>
        <View style = {styles.heading}><Text style = {{fontSize : 30, fontWeight : "bold"}}>How It Works</Text></View>
        <View style = {styles.headingsContainer}><Text style = {styles.headings}>Summary</Text></View>
        <View style = {styles.content}>

          <Text style = {styles.text}>This is an app that makes your mobile a type of a transmitter,
          and your spectators phone as a reciever.</Text>
        </View>
        <View style = {styles.headingsContainer}>
          <Text style = {styles.headings}>The Trick</Text>
        </View>
        <View style = {styles.content}>
          <Text style = {styles.text}>You ask your spectator for their phone and ask them to turn on
          chrome or safari and hand you the phone. You tell them that you are
          going to google search a famous personality, when done you place the
          phone face down on their hand or on a table and take out a deck of cards
          to proceed with the trick. You show them that each card has a name of
          a famous personailty written on it and that every card has a different name
          written on it. You hand them the deck and from here you NEVER touch the deck.
          You ask them to shuffle the deck and when they are satisfied, ask them to cut the deck from
          anywhere they want. When done ask them to see what personality name they cut to.
          Then you remind them that before starting you made a google search on their phone.
          And when they check their phone, their chrome/safari has  google search made
          with the exact personality name, they cut to on the deck
          </Text>
        </View>


        <View style = {styles.headingsContainer}>
          <Text style = {styles.headings}>The Secret</Text>
        </View>
        <View style = {styles.content}>
          <Text style = {styles.text}>If you have been into magic for a few years, you probably might have
          some idea of how such an effect can be achieved, your only concern might be
          how to make a prediction on the spectators phone. And yes this app exactly
          solves this problem. This app allows you to send information of what card
          the spectator cut to so that on the spectators phone their chrome/safari can
          get that information and loads the appropriate google search page with the right name
          of the celebrity</Text>
        </View>


        <View style = {styles.headingsContainer}>
          <Text style = {styles.headings}>More Details</Text>
        </View>
        <View style = {styles.content}>
          <Text style = {styles.text}>The trick works on a principle of breather crimp. Dont worry
          if you dont know what the breather crimp is and how it works, there is
          a video for it on the "Tips" section at the bottom of the page.</Text>
          <Text style = {styles.text}>
          For the trick you would need to apply breather crimps on four playing cards
          and mark them at the back with whatever your favourite marking method is so that
          you can see what card, the spectator has cut to. These crimped cards would be the cards
          you would force.</Text>
          <Text style = {styles.text}> Write the appropriate names of celebrities on them, that you would like to
          force on your spectator. With four crimped cards evenly spaced in the deck, the spectator
          can do a over hand shuffle or any shuffle that does not affect the crimps and when he/she
          cuts. They will always cut to one of those four cards. From here all you have to do is look at
          the marking to see what card is there and send its value to their phone from your phone thats in
          your pocket by just double tapping a certain part of the screen and you are done.
            </Text>
        </View>


        <View style = {styles.headingsContainer}>
          <Text style = {styles.headings}>How The App Works</Text>
        </View>
        <View style = {styles.content}>
          <Text style = {{marginBottom : 5}}>The app consists of four pages.</Text>
          <View style = {{flexDirection : "row"}}>
            <Ionicons name = "ios-home"  size = {30} color = "blue"/>
            <Text style = {{marginLeft : 8, marginRight : 8, fontWeight : "bold"}}>Home:</Text>
            <Text style = {styles.text}> This is the page that lets you launch the app.
            Once you have saved the personality names that you want to force in the
            settings page, this page will display your website link that you will
            have to remember and this is the link you would have to go to, in the browser, when
            you take the spectators phone on the pretext of making a google search, and leave
            your spectators phone running this link.
            When you send a input from your
            mobile, that link on the spectators phone recieves that information and will
            load a google search page accordingly and with no trace that you initially visted a
            different page. More on how to send an input later. This page also has a launch button. This when
            hit will activate your link ie That link would become live and you can visit
            it from any browser. With the app not activated if you visit that link you would not
            see any page. With the launch of the app the input screen is also launched. </Text>
          </View>
        </View>

        <View style = {styles.content}>
          <View style = {{marginTop : 8, flexDirection : "row"}}>
            <View>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
              <View style = {styles.inputIconRow}>
                <View style = {styles.inputIconSection}></View>
                <View style = {styles.inputIconSection}></View>
              </View>
            </View>
            <Text style = {{marginLeft : 8, marginRight : 8, fontWeight : "bold"}}>Input:</Text>
            <Text style = {styles.text}>This is the input page. You can load this screen only via the launch
            button on the home tab. Once on this tab the phone will not go to sleep and
            you can keep your phone in your pocket with this screen on and keep performing
            for hours. This page shows four black panels enclosed with white borders as follows
            </Text>
          </View>
        </View>


        <View style = {styles.content}>
          <View style = {{marginTop : 5, marginBottom : 5}}>
            <View style = {styles.inputIconRow}>
              <View style = {styles.inputSection}></View>
              <View style = {styles.inputSection}></View>
            </View>
            <View style = {styles.inputIconRow}>
              <View style = {styles.inputSection}></View>
              <View style = {styles.inputSection}></View>
            </View>
          </View>

          <Text style = {styles.text}>Each black square corresponds to one card value that you will
          save in the settings tab as your force cards.</Text>
        </View>



        <View style = {styles.content}>
          <View style = {{marginTop : 5, marginBottom : 5}}>
            <View style = {styles.inputIconRow}>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 1</Text></View>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 2</Text></View>
            </View>
            <View style = {styles.inputIconRow}>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 3</Text></View>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 4</Text></View>
            </View>
          </View>

          <Text style = {styles.text}>To send an input to the spectators phone all you have to do is just double
          tab the corresponding square. For example if in the settings card 2 is saved as
           "Tom Hanks" and the spectator cuts the deck to the card that has "Tom Hanks" written
           on it, you just need to double tab the card 2 square. For confirmation that your input is
           indeed recieved by the spectators browser your phone will vibrate for 1 second and thats
           how you can know that everything is going as planned while the phone is in
           your pocket.</Text>

           <View style = {{marginTop : 5, marginBottom : 5}}>
             <View style = {styles.inputIconRow}>
               <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 1</Text></View>
               <View style = {styles.inputSelected}><Text style = {styles.inputSectionWord}>Card 2</Text></View>
             </View>
             <View style = {styles.inputIconRow}>
               <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 3</Text></View>
               <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 4</Text></View>
             </View>
           </View>

        </View>


        <View style = {styles.content}>
          <Text style = {styles.text}>Now the question how to know which square is where when the
          phone is in your pocket? Thats why the white border lines are there for.
          All you need is to launch the input screen and then cover those white
          lines with two rubber bands and you would know where each square
          starts and ends while the phone is in your pocket.</Text>

          <Text style = {{fontSize : 18, marginTop : 20}}>To deactivate the app from the input
          screen all you need to do, is swipe right on the square that corresponds to
          card 1, that is the top left square and the app will return to homepage and
          your link would not be active to be used in a browser anyomre</Text>
        </View>



        <View style = {styles.content}>
          <View style = {{marginTop : 5, marginBottom : 5}}>
            <View style = {styles.inputIconRow}>
              <View style = {styles.inputSection}>
                 <Text style = {styles.inputSectionWord}>Card 1</Text>
                 <Image
                   style = {{width : 60, height : 60, resizeMode : "stretch"}}
                   source = {require("./arrow.png")}/>
              </View>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 2</Text></View>
            </View>
            <View style = {styles.inputIconRow}>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 3</Text></View>
              <View style = {styles.inputSection}><Text style = {styles.inputSectionWord}>Card 4</Text></View>
            </View>
          </View>

        </View>


        <View style = {styles.content}>
          <View style = {{flexDirection : "row", marginTop : 20}}>
            <Ionicons name = "md-settings"  size = {30} color = "blue"/>
            <Text style = {{marginLeft : 8, marginRight : 8, fontWeight : "bold"}}>Settings:</Text>
            <Text style = {styles.text}>This is the page that lets you
             save the name of the people that you want to force on your spectator via
             the crimped cards. When saving please pay extra attention to what
             the spelling of the name you want to save, actually is. Whatever you
             save here, will appear as a google search page when the app is
             activated. For example if you wanted to save "Tom Hanks" and you
             made a typo and saved "Tom Tanks", "Tom Tanks" is what you will
             see in the spectators phone when the app is activated and that
             would not be a good trick. </Text>
          </View>


        </View>




        <View style = {styles.content}>
           <View style = {{flexDirection : "row", marginTop : 20}}>
             <Ionicons name = "ios-information-circle"  size = {30} color = "blue"/>
             <Text style = {{marginLeft : 8, marginRight : 8, fontWeight : "bold"}}>Info:</Text>
             <Text style = {styles.text}>And Lastly the Info page, which is this
              current page</Text>
           </View>


        </View>


        <View style = {styles.headingsContainer}>
          <Text style = {styles.headings}>Additional Tips</Text>
        </View>
        <View style = {styles.content}>
          <Text style ={styles.text}>Incase if you dont know what a breather crimp is,
          and how it works ,here is a link for it</Text>
          <TouchableOpacity style = {{margin : 15}}onPress = {this.crimpVideo}>
            <Text style = {{color : "blue", textDecorationLine : "underline"}}>Since this is just a prototype I have not shot a video, but you can google search abt breather crimps and find tutorials on it</Text>
          </TouchableOpacity>
          <Text style = {styles.text}>While you have entered your link on the spectators phone and it
          is waiting for an input from your mobile, it is important that
          the spectators phone does not go to sleep so before starting the trick just ask them
          causally to set it up so that it does not go to sleep.</Text>
          <Text style = {styles.text}>The sytem is designed in such a way that you can activate the app just
          once and can perform the trick multiple times but keep in mind that you
          can not do it for two people at once. That is you can not have your web link
          running on two different phones at the same time.</Text>
          <Text style = {styles.text}>The link page that you visit on the spectators phone
          is designed in such a way that it looks for an input send from the mobile after every
          3 seconds. So after sending the input you will need a misdirection of atleast 3 to 5 seconds
          before asking them to turn over their phones from their hands or on from the table.</Text>
          <View style = {{marginBottom : 25}}>
            <Text style ={styles.text}>You can certainly do this trick with a regular
            playing cards deck and write different names of famous personalities on
            the faces but I would suggest to use a blank faced deck and write the names on
            that. This would look more elegant and professional.</Text>
          </View>

        </View>

      </ScrollView>
    )
  }
}


const styles = StyleSheet.create({
  container : {
    //flex : 1,
    alignItems : "center",
    // justifyContent : "center",
    backgroundColor : "#fff",
    marginTop : Constants.statusBarHeight,

  },
  heading : {
    //backgroundColor : "red",
    alignItems : "center",
    justifyContent : "center"
  },

  content : {

    marginTop : 8,
    alignItems : "center",
    justifyContent : "center",
    width : "80%",
    backgroundColor : "#fff"
  },

  text : {
    fontSize : 18,
    padding : 5
  },

  headings : {
    fontWeight : "bold",
    alignSelf : "flex-start",
    fontSize : 22,
  },

  headingsContainer : {
    width : "80%",
    backgroundColor: "pink",
    alignItems : "center",
    marginTop : 10,
  },

  inputIconRow : {
    flexDirection : "row"
  },

  inputIconSection : {
    width : 16,
    height : 16,
    backgroundColor : "black",
    borderWidth : 1,
    borderColor : "white",
  },

  inputSection : {
    width : 150,
    height : 150,
    backgroundColor : "black",
    borderWidth : 2,
    borderColor : "white",
    justifyContent : "center",
    alignItems : "center",
  },

  inputSectionWord : {
    color : "#fff",

  },

  inputSelected : {
    width : 150,
    height : 150,
    backgroundColor : "blue",
    borderWidth : 2,
    borderColor : "white",
    justifyContent : "center",
    alignItems : "center",
  },
})
